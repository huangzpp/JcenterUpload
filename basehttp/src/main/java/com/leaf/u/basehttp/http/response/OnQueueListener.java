package com.leaf.u.basehttp.http.response;

import com.leaf.u.basehttp.http.request.AsyncCall;

import java.util.List;

/**
 * Created by bz on 2017/6/28.
 */

public interface OnQueueListener {
    void onProceed(AsyncCall call, int finish, int unfinish);

    void onCompled(List<AsyncCall> call);
}
