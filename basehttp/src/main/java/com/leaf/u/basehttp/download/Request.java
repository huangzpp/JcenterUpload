package com.leaf.u.basehttp.download;

public class Request {
	private String url;
	private Headers headers;

	public Request(String url, Headers headers) {
		super();
		this.url = url;
		this.headers = headers;
	}

	public String url() {
		return this.url;
	}

	public Headers headers() {
		return this.headers;
	}

	public static class Builder {
		private String url;
		private Headers.Builder headers;

		public Builder() {
			headers = new Headers.Builder();
		}

		public Request build() {

			return new Request(url, headers.build());
		}

		public Builder setUrl(String url) {
			this.url = url;
			return this;
		}

		public Builder addHeader(String name, String value) {
			headers.add(name, value);
			return this;
		}
	}
}
