package com.leaf.u.basehttp.download;

public enum State {
    PAUSE,//暂停
    LOADING,//读取中
    FAILURE,//失败
    SUCCESS,//完成
    START,//任务开始
    SUSPENDED,//任务挂起
    DELETED,//任务被移除
    OPEN,//
    ENQUEUE,//进入任务队列
    STANDBY,//等待进入队列
}