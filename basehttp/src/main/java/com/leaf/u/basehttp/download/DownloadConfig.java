package com.leaf.u.basehttp.download;

/**
 * Created by bz on 2017/4/6.
 */

public class DownloadConfig {
    public static final int NETWORK_MOBILE = 0x001;
    public static final int NETWORK_WIFI = 0x002;
    public static final int NETWORK_BOTH = 0x003;
    public static final int NETWORK_MASK = 0x001;
    private int NETWORK = NETWORK_BOTH;

    public void configNetwork(int type) {
        NETWORK = type;
    }
    public int networkType(){
        return NETWORK;
    }
}
