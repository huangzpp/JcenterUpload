package com.leaf.u.basehttp.http;

import com.alibaba.fastjson.JSON;
import com.leaf.u.basehttp.http.response.LeafResponse;

import java.lang.reflect.Type;

public class DefaultConvert implements ResponseConvert {
    @Override
    public <Output> Output convert(Type type, LeafResponse response) {
        return JSON.parseObject(response.original(), type);
    }
}
