package com.leaf.u.basehttp.download;

public enum Error {
	OutOfDiskSpace, NetWorkError, Other
}
