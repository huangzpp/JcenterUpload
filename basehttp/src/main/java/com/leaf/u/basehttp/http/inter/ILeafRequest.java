package com.leaf.u.basehttp.http.inter;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * Created by bz on 2017/5/27.
 */

public interface ILeafRequest<Body> {

    ILeafRequest get();

    ILeafRequest post();

    ILeafRequest url(String url);

    ILeafRequest put(String key, String value);

    ILeafRequest put(String key, long value);

    ILeafRequest put(String key, File file);

    ILeafRequest array(String key, List<File> file);

    ILeafRequest array(String key, String[] value);

    ILeafRequest putExtra(String key, Object object);

    ILeafRequest addHeader(String key, String value);

    Map<String, Object> getParams();

    Map<String, String> getHeaders();

    Map<String, Object> getExtras();

    Object getExtra(String key);

    String getString(String key);

    long getLong(String key);

    Body build();
}
