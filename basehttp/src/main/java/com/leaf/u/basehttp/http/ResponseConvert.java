package com.leaf.u.basehttp.http;

import com.leaf.u.basehttp.http.response.LeafResponse;

import java.lang.reflect.Type;

public interface ResponseConvert {
    <Output> Output convert(Type type, LeafResponse response);
}
