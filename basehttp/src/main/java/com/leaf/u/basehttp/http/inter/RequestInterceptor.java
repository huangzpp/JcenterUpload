package com.leaf.u.basehttp.http.inter;

import com.leaf.u.basehttp.http.HttpEngine;
import com.leaf.u.basehttp.http.okimpl.LeafRequest;
import com.leaf.u.basehttp.http.response.LeafResponse;

import java.io.IOException;

/**
 * Created by bz on 2017/6/7.
 * 响应之前的拦截器
 */


public interface RequestInterceptor {
    LeafResponse interceptor(Chain chain) throws IOException;

    interface Chain {
        LeafRequest request();

        int getLooper();

        HttpEngine engine();

        LeafResponse proceed(LeafRequest request) throws IOException;

        void termination();
    }
}
