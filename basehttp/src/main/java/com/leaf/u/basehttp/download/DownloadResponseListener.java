package com.leaf.u.basehttp.download;

public interface DownloadResponseListener<T> {
	String updateURL();

	void onStart(T info);

	void onFailure(T info);

	void onProgress(T info);

	void onPause(T info);

	void onSuccess(T info);

	void onDelete(T info);

	void enQueue(T info);

	void suspended(T info);
}
