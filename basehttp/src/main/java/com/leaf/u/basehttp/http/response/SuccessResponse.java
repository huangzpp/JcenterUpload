package com.leaf.u.basehttp.http.response;

import com.leaf.u.basehttp.download.Headers;

/**
 * Created by bz on 2016/9/5.
 */
public class SuccessResponse<T> {
    private T body;
    private Headers headers;
    private String original;

    private SuccessResponse(T body, String original, Headers headers) {
        this.body = body;
        this.original = original;
    }

    public T body() {
        return body;
    }

    public String original() {
        return original;
    }

    public Headers headers() {
        return headers;
    }


    public static class Builder<T> {
        private T body;
        private Headers headers;
        private String original;


        public Builder setHeaders(Headers headers) {
            this.headers = headers;
            return this;
        }

        public Builder setBody(T body) {
            this.body = body;
            return this;
        }

        public Builder setOriginal(String original) {
            this.original = original;
            return this;
        }

        public SuccessResponse build() {
            return new SuccessResponse(body, original, headers);
        }
    }
}
