package com.leaf.u.basehttp.download;

import java.io.IOException;

public interface IHttp {
	Response get(Request r) throws IOException;
}
