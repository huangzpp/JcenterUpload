package com.leaf.u.basehttp.download;

public interface DownloadListener<T extends DownloadModel> {
	void onConnect(DownloadManager.ConnectionType type, T model);

	void onAddTask(int active, int total, T model);

	void onDelTask(int active, int total, T model);

	void onFinish(int finish, int total, T model);

	void onTaskLoading(int num, T model);

	void onPauseTask(T model);

	void onNetChange(DownloadManager.ConnectionType type);

	void onFailure(T model);
	void onDownloadTaskCount(int count);
}
