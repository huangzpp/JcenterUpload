package com.leaf.u.basehttp.http;


import com.leaf.u.basehttp.http.inter.RequestInterceptor;
import com.leaf.u.basehttp.http.okimpl.LeafRequest;
import com.leaf.u.basehttp.http.request.AsyncCall;
import com.leaf.u.basehttp.http.request.LeafCallQueue;

import okhttp3.OkHttpClient;

/**
 * Created by bz on 2016/11/30.
 */

public class LeafHttpClient {
    private static LeafHttpClient instance;
    private HttpEngine httpEngine;

    private LeafHttpClient(OkHttpClient client) {
        httpEngine = new HttpEngine(client);
    }

    public void addInterceptor(RequestInterceptor interceptor) {
        httpEngine.addInterceptor(interceptor);
    }

    public static LeafHttpClient create(){
        return create(null);
    }
    public static LeafHttpClient create(OkHttpClient client) {
        if (instance == null) {
            instance = new LeafHttpClient(client);
        }
        return instance;
    }

    public AsyncCall newCall(LeafRequest request) {
        return new AsyncCall(httpEngine, request);
    }

    public LeafCallQueue newQueue() {
        return new LeafCallQueue(httpEngine);
    }


}
