package com.leaf.u.basehttp.http.response;

/**
 * Created by bz on 2016/9/5.
 */
public class ErrorResponse {
    private int code;
    private String original;
    private Throwable throwable;

    private ErrorResponse(int code, String original, Throwable throwable) {
        this.code = code;
        this.original = original;
        this.throwable = throwable;
    }

    public int code() {
        return code;
    }

    public Throwable throwable() {
        return throwable;
    }

    public String original() {
        return this.original;
    }

    public static class Builder {
        private int code;
        private String original;
        private Throwable throwable;

        public Builder setCode(int code) {
            this.code = code;
            return this;
        }

        public Builder setOriginal(String original) {
            this.original = original;
            return this;
        }

        public Builder setThrowable(Throwable throwable) {
            this.throwable = throwable;
            return this;
        }

//        public Builder setType(OnHttpResponse.Error type) {
//            this.type = type;
//            return this;
//        }
//
//        public Builder setMsg(String msg) {
//            this.msg = msg;
//            return this;
//        }

        public ErrorResponse build() {
            return new ErrorResponse(code, original, throwable);
        }

    }


}
