package com.leaf.u.basehttp.download;

import android.app.Service;

/**
 * Created by bz on 2017/4/6.
 */

public abstract class DownloadService extends Service {
    @Override
    public void onCreate() {
        NetWorkStateReceiver.register(this);
        super.onCreate();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        NetWorkStateReceiver.unregister(this);
    }
    public abstract DownloadManager getDownloadManager();
}
