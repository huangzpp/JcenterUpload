package com.leaf.u.basehttp.http.response;

import java.lang.reflect.Type;

public interface OnHttpResponse<T> {
	enum Error {
		parseError, //
		httpError, //
		nullError, // Request data is null
		logicError//
	}

	void onSuccess(SuccessResponse<T> response);

	void onFailed(ErrorResponse response);

	Type getType();
//	Class getEntityClass();
}
