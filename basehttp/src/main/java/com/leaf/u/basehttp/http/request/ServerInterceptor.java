package com.leaf.u.basehttp.http.request;

import com.leaf.u.basehttp.http.HttpEngine;
import com.leaf.u.basehttp.http.inter.RequestInterceptor;
import com.leaf.u.basehttp.http.okimpl.LeafRequest;
import com.leaf.u.basehttp.http.response.LeafResponse;

import java.io.IOException;

/**
 * Created by bz on 2017/6/7.
 */

public class ServerInterceptor implements RequestInterceptor {
    @Override
    public LeafResponse interceptor(Chain chain) throws IOException {
        LeafRequest request = chain.request();
        HttpEngine engine = chain.engine();
        LeafResponse leafResponse = engine.async_request(request);
        return leafResponse;
    }
}
