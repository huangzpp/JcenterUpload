package com.leaf.u.basehttp.download;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Parcelable;
import android.util.Log;

/**
 * Created by bz on 2017/3/30.
 */

public class NetWorkStateReceiver extends BroadcastReceiver {
    public static DownloadManager manager;
    public static NetWorkStateReceiver netWorkStateReceiver;

    public static void register(DownloadService context) {
        if (netWorkStateReceiver == null) {
            netWorkStateReceiver = new NetWorkStateReceiver();
            manager = context.getDownloadManager();
        }
        IntentFilter filter = new IntentFilter();
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        filter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        context.registerReceiver(netWorkStateReceiver, filter);
    }

    public static void unregister(DownloadService context) {
        context.unregisterReceiver(netWorkStateReceiver);
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        //网络连接状态改变广播
        if (ConnectivityManager.CONNECTIVITY_ACTION.equals(intent.getAction())) {
            ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo == null) {
                manager.onNetWorkChange(DownloadManager.ConnectionType.TYPE_NONE);
            } else if (networkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                manager.onNetWorkChange(DownloadManager.ConnectionType.TYPE_WIFI);
//            Toast.makeText(context,"WIFI"+networkInfo.isConnected(),Toast.LENGTH_SHORT).show();
                Log.e("NETWORK", "ISCONNECTED:" + networkInfo.isConnected());
                Log.e("NETWORK", "ISAVAILABLE" + networkInfo.isAvailable());
            } else if (networkInfo.getType() == ConnectivityManager.TYPE_MOBILE) {

                manager.onNetWorkChange(DownloadManager.ConnectionType.TYPE_MOBILE);
            } else {
                manager.onNetWorkChange(DownloadManager.ConnectionType.TYPE_OTHER);
            }

        }
        if (WifiManager.NETWORK_STATE_CHANGED_ACTION.equals(intent.getAction())) {
            Parcelable parcelableExtra = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
            if (null != parcelableExtra) {
                NetworkInfo networkInfo = (NetworkInfo) parcelableExtra;
                NetworkInfo.State state = networkInfo.getState();
                boolean isConnected = state == NetworkInfo.State.CONNECTED;// 当然，这边可以更精确的确定状态
                Log.e("NETWORK", "isConnected" + isConnected+"  DetailState:"+networkInfo.getDetailedState());


                if (isConnected) {
                } else {

                }
            }
        }
    }
}
