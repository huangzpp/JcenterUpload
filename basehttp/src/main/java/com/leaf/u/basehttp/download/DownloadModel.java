package com.leaf.u.basehttp.download;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.io.Serializable;

@Table(name = "Download")
public class DownloadModel extends Model{
    @Column(name = "url", unique = true)
    private String url;
    @Column(name = "filename")
    private String filename;
    @Column(name = "path")
    private String absolutePath;
    @Column(name = "length")
    private long length;
    @Column(name = "status")
    private State status;
    @Column(name = "curLength")
    private long curLength;
    @Column(name = "error")
    private Error error;

    private float speed;

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public void setCurLength(long curLength) {
        this.curLength = curLength;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getAbsolutePath() {
        return absolutePath;
    }

    public void setAbsolutePath(String absolutePath) {
        this.absolutePath = absolutePath;
    }

    public long getLength() {
        return length;
    }

    public void setLength(long length) {
        this.length = length;
    }

    public State getStatus() {
        return status;
    }

    public void setStatus(State status) {
        this.status = status;
    }

    public long getCurLength() {
        return curLength;
    }

    public void setCurLenght(long curLenght) {
        this.curLength = curLenght;
    }

}
