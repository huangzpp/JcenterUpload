package com.leaf.u.basehttp.http;

import com.leaf.u.basehttp.http.response.OnHttpResponse;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * Created by bz on 2016/12/14.
 */

public abstract class HttpCallback<T> implements OnHttpResponse<T> {
    //    private Class<T> entityClass;
    private Type type;

    public HttpCallback() {
        Type genType = getClass().getGenericSuperclass();
        Type[] params = ((ParameterizedType) genType).getActualTypeArguments();
        type = params[0];
//        entityClass = (Class) params[0];
    }

    //    public Class<T> getEntityClass() {
//        return this.entityClass;
//    }
    public Type getType() {
        return this.type;
    }

}
