package com.leaf.u.basehttp.http.request;

/**
 * Created by bz on 2017/5/18.
 */

public interface AsyncTaskListener {
    void onSuccess(AsyncCall call);
}
