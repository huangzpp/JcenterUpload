package com.pumpkin.baseui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bz on 2016/11/29.
 */

public abstract class BaseRecycleAdapter<K, T extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<T> {
    private LayoutInflater mInflater;
    private final Context context;
    private final List<K> mDatas = new ArrayList<K>();

    public BaseRecycleAdapter(Context context) {
        this.context = context;
        mInflater = LayoutInflater.from(context);
    }
    public K get(int position) {
        try {
            K result = mDatas.get(position);
            return result;
        } catch (Exception e) {
        }
        return null;
    }
    public void remove(K object) {
        mDatas.remove(object);
        notifyDataSetChanged();
    }
    public void setDatas(List<K> data) {
        mDatas.clear();
        if (data != null && data.size() > 0) {
            mDatas.addAll(data);
        }
        notifyDataSetChanged();
    }

    public void addToDatas(List<K> data) {
        if (data != null && data.size() > 0) {
            mDatas.addAll(data);
            notifyDataSetChanged();
        }
    }

    public void addDataToFirst(K data) {
        mDatas.add(0, data);
        notifyDataSetChanged();
    }

    public boolean hasData() {
        return mDatas.size() > 0;
    }
    public Context getContext() {
        return this.context;
    }
    public abstract T onCreateViewHolder(LayoutInflater inflater,ViewGroup parent,int viewType);

    @Override
    public T onCreateViewHolder(ViewGroup parent, int viewType) {
        return onCreateViewHolder(mInflater,parent,viewType);
    }
    @Override
    public int getItemCount() {
        return mDatas.size();
    }
}
