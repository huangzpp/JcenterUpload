package com.pumpkin.baseui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseHolderAdapter<K, VHolder extends BaseHolderAdapter.ViewHolder> extends BaseAdapter {

    private LayoutInflater mInflater;
    private Context mContext;
    private List<K> mDatas = new ArrayList<K>();
    private Context mFragment;

    public BaseHolderAdapter(Context context) {
//        if (fragment == null) {
//            return;
//        }
        this.mContext = context;
//        this.mContext = fragment.getActivity();
        mInflater = LayoutInflater.from(mContext);
    }

//    public void startFragment(PrepareBaseFragment fragment) {
//        ((SupportActivity) mContext).start(fragment);
////        mFragment.start(fragment);
//    }

    public K get(int position) {
        try {
            K result = mDatas.get(position);
            return result;
        } catch (Exception e) {
        }
        return null;
    }
//    public PrepareBaseFragment getFragment(){
//        return mFragment;
//    }

    public Context getContext() {
        return mContext;
    }

    public void remove(K object) {
        mDatas.remove(object);
        notifyDataSetChanged();
    }

    public abstract class ViewHolder {
        View view;

        public ViewHolder(View view) {
            this.view = view;
        }

        public abstract void update(int position);
    }

    public void setDatas(List<K> data) {
        mDatas.clear();
        if (data != null && data.size() > 0) {
            mDatas.addAll(data);
        }
        notifyDataSetChanged();
    }

    public void addToDatas(List<K> data) {
        if (data != null && data.size() > 0) {
            mDatas.addAll(data);
            notifyDataSetChanged();
        }
    }

    public void addData(K data, int position, boolean clears) {
        if (clears) {
            mDatas.clear();
        }
        mDatas.add(position, data);
        notifyDataSetChanged();
    }

    public void addDataToFirst(K data) {
        mDatas.add(0, data);
        notifyDataSetChanged();
    }

    /**
     * @param data       数据
     * @param repeatable 可重复的
     */
    public void addData(K data, boolean repeatable) {
        if (!repeatable && mDatas.contains(data)) {
            return;
        }
        mDatas.add(data);
        notifyDataSetChanged();
    }

    public boolean hasData() {
        return mDatas.size() > 0;
    }

    @Override
    public int getCount() {
        return mDatas.size();
    }

    @Override
    public Object getItem(int position) {
        return mDatas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    protected View inflate(int resid) {
        return mInflater.inflate(resid, null);
    }

    @SuppressWarnings("unchecked")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        VHolder holder;
        if (convertView == null) {
            holder = oncreateViewHolder(parent, getItemViewType(position));
            convertView = holder.view;
            convertView.setTag(holder);
        } else {
            holder = (VHolder) convertView.getTag();
        }
        holder.update(position);
        return convertView;
    }

    protected abstract VHolder oncreateViewHolder(ViewGroup parent, int viewType);
}
