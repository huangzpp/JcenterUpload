package com.yhotbms.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout

typealias Callback = (View, Int) -> Unit

class SimpleSelect : LinearLayout {

    private val INVALID_POSITION = -1
    private var mOnTabClickListener: OnClickListenerTab? = null
    private var mLastTabView: View? = null
    private var mOnTabSelectedListener: Callback = { v, position -> }
    private var mPosition = DEFAULT_SELECT

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        setOnHierarchyChangeListener(object : OnHierarchyChangeListener {

            override fun onChildViewRemoved(parent: View, child: View) {

            }

            override fun onChildViewAdded(parent: View, child: View) {
                if (mOnTabClickListener == null) {
                    mOnTabClickListener = OnClickListenerTab()
                }
                val position = getPositionForView(child)
                child.setOnClickListener(mOnTabClickListener)
                if (position == DEFAULT_SELECT) {
                    setTabSelect(position)
                }
            }
        })
    }

    constructor(context: Context) : super(context)

    internal inner class OnClickListenerTab : OnClickListener {
        override fun onClick(v: View) {
            mPosition = getPositionForView(v)
            setTabSelect(mPosition)
        }
    }

    fun clearFoucs() {
        // TODO
        if (mLastTabView != null) {
            mLastTabView!!.isSelected = false
            mLastTabView = null
        }
    }

    /**
     * 不调用回调方法
     * @param position
     */
    fun setTabSelectSilence(position: Int) {
        if (mLastTabView != null) {
            mLastTabView!!.isSelected = false
        }
        var v: View? = null
        try {
            v = getChildAt(position)
            if (v != null) {
                v.isSelected = true
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            mLastTabView = v
        }
    }

    fun setTabSelect(position: Int) {
        if (mLastTabView != null) {
            mLastTabView!!.isSelected = false
        }
        var v: View? = null
        try {
            v = getChildAt(position)
            if (v != null) {
                v.isSelected = true
                if (mOnTabSelectedListener != null && mLastTabView !== v) {
                    mOnTabSelectedListener(v, position)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            mLastTabView = v
        }
    }

    fun getPositionForView(view: View): Int {
        var listItem = view
        try {
            val value = listItem.parent as View
            while (value != this) {
                listItem = value
            }
        } catch (e: ClassCastException) {
            return INVALID_POSITION
        }

        val childCount = childCount
        for (i in 0 until childCount) {
            if (getChildAt(i) == listItem) {
                return i
            }
        }

        return INVALID_POSITION
    }


    fun setOnTabSelectedListener(listener: Callback) {
        this.mOnTabSelectedListener = listener
    }

    companion object {
        private val DEFAULT_SELECT = 0
    }

}
