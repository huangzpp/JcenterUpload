package com.example.zp.jcenterupload.widget.chart;

/**
 * Created by zp on 2017/11/30.
 */

public class Label {
    private float x;
    private float y;
    private String label;
    private float length;

    public void setLength(float length) {
        this.length = length;
    }

    public float getLength() {
        return this.length;
    }

    public void set(float x, float y, String label) {
        this.x = x;
        this.y = y;
        this.label = label;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
