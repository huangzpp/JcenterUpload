package com.example.zp.jcenterupload.socket;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

public class UdpSocketExt {
    private static int udpPort;
    private static String hostIp;
    private static DatagramSocket socket = null;
    private static DatagramPacket packetSend;
    private static DatagramPacket packetRcv;
    private byte[] msgRcv = new byte[1024]; //接收消息
    private byte[] Buffer_Send = new byte[1024];
    private MessageRecvListener messageRecvListener;

    // TODO: 2017/12/5 这边要传入一个MessageRecvListener用来接收消息
    public UdpSocketExt(String ipaddr, int port, MessageRecvListener l) {
        hostIp = ipaddr;
        udpPort = port;
        messageRecvListener = l;

        try {
            socket = new DatagramSocket(udpPort);
            packetRcv = new DatagramPacket(msgRcv, msgRcv.length);
            packetSend = new DatagramPacket(Buffer_Send, 1024);
            Thread_Recv thread_recv = new Thread_Recv();
            thread_recv.start();

        } catch (SocketException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        packetSend.setPort(udpPort);
        try {
            packetSend.setAddress(InetAddress.getByName(hostIp));
        } catch (UnknownHostException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }


    public void send(byte[] data, int len) {
        packetSend.setData(data);
        packetSend.setLength(len);
        Thread_Send thread_send = new Thread_Send();
        thread_send.start();
    }

    /**
     * @brief 发送线程
     */
    private class Thread_Send extends Thread {
        @Override
        public void run() {
            try {
                socket.send(packetSend);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//			try {
//				
//				socket.receive(packetRcv);
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//
//			for (int i =0; i < packetRcv.getLength(); i++){
//				Log.e("jim", Integer.toHexString(packetRcv.getData()[i]));
//		    }
        }
    }


    private class Thread_Recv extends Thread {
        //TODO 定义一个handler在主线程中处理消息
        private Handler handler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                //将数据发送给接口（接口由调用方实现）
                if (messageRecvListener != null) {
                    messageRecvListener.onRecv(msg.obj.toString());
                }
            }
        };

        public void run() {
            while (true) {
                try {

                    socket.receive(packetRcv);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //TODO 将消息放到buffer里面
                StringBuffer buffer = new StringBuffer();
                for (int i = 0; i < packetRcv.getLength(); i++) {
                    buffer.append(Integer.toHexString(packetRcv.getData()[i]));
                    Log.e("jim", Integer.toHexString(packetRcv.getData()[i]));
                }
//                将buffer放到Message发送到handler
                Message msg = new Message();
                msg.obj = buffer;
                handler.sendMessage(msg);
            }
        }
    }

    public void closeSocket() {
        socket.close();
    }

    public byte CT_ZoomIn() {
        byte[] cmd = new byte[6];
        cmd[0] = (byte) 0x81;
        cmd[1] = (byte) 0x01;
        cmd[2] = (byte) 0x04;
        cmd[3] = (byte) 0x07;
        cmd[4] = (byte) 0x27;
        cmd[5] = (byte) 0xFF;
        send(cmd, cmd.length);
        return 0;
    }

    public byte CT_UdpZoomOut() {
        byte[] cmd = new byte[6];
        cmd[0] = (byte) 0x81;
        cmd[1] = (byte) 0x01;
        cmd[2] = (byte) 0x04;
        cmd[3] = (byte) 0x07;
        cmd[4] = (byte) 0x37;
        cmd[5] = (byte) 0xFF;
        send(cmd, cmd.length);
        return 0;
    }

    public byte CT_UdpZoomStop() {
        byte[] cmd = new byte[6];
        cmd[0] = (byte) 0x81;
        cmd[1] = (byte) 0x01;
        cmd[2] = (byte) 0x04;
        cmd[3] = (byte) 0x07;
        cmd[4] = (byte) 0x00;
        cmd[5] = (byte) 0xFF;
        send(cmd, cmd.length);
        return 0;
    }

    public byte CT_UdpFocusFar() {
        byte[] cmd = new byte[6];
        cmd[0] = (byte) 0x81;
        cmd[1] = (byte) 0x01;
        cmd[2] = (byte) 0x04;
        cmd[3] = (byte) 0x08;
        cmd[4] = (byte) 0x27;
        cmd[5] = (byte) 0xFF;
        send(cmd, cmd.length);
        return 0;
    }

    public byte CT_UdpFocusNear() {
        byte[] cmd = new byte[6];
        cmd[0] = (byte) 0x81;
        cmd[1] = (byte) 0x01;
        cmd[2] = (byte) 0x04;
        cmd[3] = (byte) 0x08;
        cmd[4] = (byte) 0x37;
        cmd[5] = (byte) 0xFF;
        send(cmd, cmd.length);
        return 0;
    }


    public byte CT_UdpFocusStop() {
        byte[] cmd = new byte[6];
        cmd[0] = (byte) 0x81;
        cmd[1] = (byte) 0x01;
        cmd[2] = (byte) 0x04;
        cmd[3] = (byte) 0x08;
        cmd[4] = (byte) 0x00;
        cmd[5] = (byte) 0xFF;
        send(cmd, cmd.length);
        return 0;
    }

    public byte CT_UdpMenuEnter() {
        byte[] cmd = new byte[6];
        cmd[0] = (byte) 0x81;
        cmd[1] = (byte) 0x02;
        cmd[2] = (byte) 0x04;
        cmd[3] = (byte) 0x07;
        cmd[4] = (byte) 0x00;
        cmd[5] = (byte) 0xFF;
        send(cmd, cmd.length);
        return 0;
    }

    public byte CT_UdpMenuUp() {
        byte[] cmd = new byte[6];
        cmd[0] = (byte) 0x81;
        cmd[1] = (byte) 0x02;
        cmd[2] = (byte) 0x04;
        cmd[3] = (byte) 0x07;
        cmd[4] = (byte) 0x02;
        cmd[5] = (byte) 0xFF;
        send(cmd, cmd.length);
        return 0;
    }

    public byte CT_UdpMenuDown() {
        byte[] cmd = new byte[6];
        cmd[0] = (byte) 0x81;
        cmd[1] = (byte) 0x02;
        cmd[2] = (byte) 0x04;
        cmd[3] = (byte) 0x07;
        cmd[4] = (byte) 0x03;
        cmd[5] = (byte) 0xFF;
        send(cmd, cmd.length);
        return 0;
    }

    public byte CT_UdpMenuLeft() {
        byte[] cmd = new byte[6];
        cmd[0] = (byte) 0x81;
        cmd[1] = (byte) 0x02;
        cmd[2] = (byte) 0x04;
        cmd[3] = (byte) 0x08;
        cmd[4] = (byte) 0x02;
        cmd[5] = (byte) 0xFF;
        send(cmd, cmd.length);
        return 0;
    }

    public byte CT_UdpMenuRight() {
        byte[] cmd = new byte[6];
        cmd[0] = (byte) 0x81;
        cmd[1] = (byte) 0x02;
        cmd[2] = (byte) 0x04;
        cmd[3] = (byte) 0x08;
        cmd[4] = (byte) 0x03;
        cmd[5] = (byte) 0xFF;
        send(cmd, cmd.length);
        return 0;
    }

    public byte CT_UdpMenuBack() {
        byte[] cmd = new byte[6];
        cmd[0] = (byte) 0x81;
        cmd[1] = (byte) 0x02;
        cmd[2] = (byte) 0x04;
        cmd[3] = (byte) 0x07;
        cmd[4] = (byte) 0x01;
        cmd[5] = (byte) 0xFF;
        send(cmd, cmd.length);
        return 0;
    }

    //TODO 定义一个接口
    public interface MessageRecvListener {
        void onRecv(String msg);
    }

    public static void main(String[] args) throws Exception {

    }
}
