package com.example.zp.jcenterupload.http;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.example.zp.jcenterupload.dialog.LoadingDialog;
import com.leaf.u.basehttp.http.inter.RequestInterceptor;
import com.leaf.u.basehttp.http.response.LeafResponse;

import java.io.IOException;

public class IndicatorInterceptor implements RequestInterceptor {

    private LoadingDialog dialog;
    private Handler handler;

    @SuppressLint("HandlerLeak")
    public IndicatorInterceptor(Context context) {
        dialog = new LoadingDialog(context);
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if(msg.what==0){
                    dialog.show();
                }else{
                    dialog.dismiss();
                }
            }
        };
    }

    @Override
    public LeafResponse interceptor(Chain chain) throws IOException {
        handler.sendEmptyMessage(0);
        LeafResponse response = null;
        try {
            response = chain.proceed(chain.request());
        } finally {
            handler.sendEmptyMessage(1);
        }
        return response;
    }
}
