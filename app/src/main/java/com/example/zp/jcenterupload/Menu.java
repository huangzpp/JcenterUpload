package com.example.zp.jcenterupload;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

/**
 * Created by jesse on 2017/10/27.
 */

public abstract class Menu {

    private View menu;

    public Menu(Context context){
        menu = onCreateMunu(LayoutInflater.from(context));
    }
    public View getMenu(){
        return menu;
    }
    protected  abstract View onCreateMunu(LayoutInflater inflater);

}
