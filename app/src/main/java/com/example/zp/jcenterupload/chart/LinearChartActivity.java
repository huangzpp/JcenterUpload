package com.example.zp.jcenterupload.chart;

import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.zp.jcenterupload.R;
import com.example.zp.jcenterupload.base.ActionBarActivity;
import com.example.zp.jcenterupload.widget.chart.LinearChart;
import com.pumpkin.navigation.ActionBar;
import com.pumpkin.navigation.ActionBarLayout;
import com.pumpkin.navigation.menu.TextMenu;

import java.util.Random;

/**
 * Created by zp on 2017/12/14.
 */
public class LinearChartActivity extends ActionBarActivity {
    private TextView mTipText;
    private String[] mstrValues = new String[]{"ad", "adff", "adfadf", "adfadfadf", "s"};

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_linear_chart);
        final LinearChart chart = (LinearChart) findViewById(R.id.linear_chart);
        findViewById(R.id.linear_refresh).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                chart.setXLabels("123", "12", "1");
                chart.setMaxValue(10);
                chart.testCase();
            }
        });
        makeValue();
        View view = LayoutInflater.from(this).inflate(R.layout.tip_demo, null);
        mTipText = (TextView) view.findViewById(R.id.tip_text);
        chart.setOnPointClickListener(new LinearChart.OnPointClickListener() {
            @Override
            public void onPointClick(Point point, float value) {
//                double random = Math.random();
                Random random = new Random();
                int i = random.nextInt(5);
                String text = mstrValues[i];
                mTipText.setText(text);
                Toast.makeText(LinearChartActivity.this, text, Toast.LENGTH_SHORT).show();
//                mTipText.setText(String.format("%.1f", value));
//                mTipText.requestLayout();
            }
        });
        chart.setTipView(view);
        chart.setAxisColor(0xFFDCDCDC);
        chart.setLineColor(0xFFFFB592);
        chart.setLineWidth(1.5f);
        chart.setPointColor(0xFFFFB592);
        chart.setAxisXTextColor(0xFF999999);
        chart.setAxisYTextColor(0xFF999999);
        chart.setProtrudingRight(0);
        chart.setValues(mValues);
        chart.setMaxValue(20);
        chart.setXLabels("1", "123", "1234");
        chart.setStepNum(4);
    }

    float mValues[] = new float[6];

    private void makeValue() {
        Random random = new Random();
        for (int i = 0; i < mValues.length; i++) {
            float v = random.nextFloat();
            mValues[i] = v * 10;
        }
    }

    @Override
    protected void onCreateActionBar(ActionBar actionBar) {
        super.onCreateActionBar(actionBar);
        TextMenu marstMenu = new TextMenu(this);
        marstMenu.setText("拆线图");
        marstMenu.setTextColor(0xFFFFFFFF);
        marstMenu.setTextSize(18);
        marstMenu.setGravity(ActionBarLayout.Gravity.CENTER);
        marstMenu.setMarginLeft(10);
        actionBar.setCustomMenu(marstMenu);
    }
}
