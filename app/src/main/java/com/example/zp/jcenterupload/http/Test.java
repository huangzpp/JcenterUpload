package com.example.zp.jcenterupload.http;

import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.leaf.u.basehttp.http.inter.RequestInterceptor;
import com.leaf.u.basehttp.http.response.LeafResponse;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Test implements RequestInterceptor {
    @Override
    public LeafResponse interceptor(Chain chain) throws IOException {
        List<UserBean> tests = new ArrayList<>();
        tests.add(new UserBean("jesse1", "18"));
        tests.add(new UserBean("jesse2", "18"));
        tests.add(new UserBean("jesse3", "18"));
        String jsonString = JSON.toJSONString(tests);
        LeafResponse response = new LeafResponse.Builder()
                .setCode(200)
                .setInputStream(new ByteArrayInputStream(jsonString.getBytes()))
                .build();
//        LeafResponse leafResponse = null;
//        try {
//            leafResponse = chain.proceed(chain.request());
//        } finally {
//            Log.e("Test", "异常之后");
//        }
        chain.termination();
        return response;
    }
}
