package com.example.zp.jcenterupload.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toolbar;

import com.example.zp.jcenterupload.R;
import com.pumpkin.navigation.ActionBar;
import com.pumpkin.navigation.ActionBarLayout;

public class ActionBarActivity extends AppCompatActivity {

    private FrameLayout mContent;
    private ActionBar actionBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if(actionBar!=null){
            actionBar.hide();
        }
//        ((Toolbar)actionBar).setVisibility(View.GONE);
        setContentView(R.layout.activity_actionbar);
        mContent = findViewById(R.id.activity_actionbar_content);
        ActionBarLayout actionBarLayout = findViewById(R.id.activity_actionbar);
        this.actionBar = new ActionBar(actionBarLayout);
        onCreateActionBar(this.actionBar);
        /*set it to be no title*/
//        /*set it to be full screen*/
    }
    protected void onCreateActionBar(ActionBar actionBar){
        actionBar.setBackgroundColor(0xFF123456);
    }

    @Override
    public void setContentView(View view) {
//        super.setContentView(view);
        mContent.addView(view);
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
//        super.setContentView(view, params);
        mContent.addView(view, params);
    }

    @Override
    public void setContentView(int layoutResID) {
//        super.setContentView(layoutResID);
        if (layoutResID != R.layout.activity_actionbar) {
            setContentView(LayoutInflater.from(this).inflate(layoutResID, null));
        } else {
            super.setContentView(layoutResID);
        }
    }
}
