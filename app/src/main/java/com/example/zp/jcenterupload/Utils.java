package com.example.zp.jcenterupload;

import android.content.Context;

/**
 * Created by zp on 2017/10/27.
 */

public class Utils {
    public static int dip2px(Context context, float dipValue) {
        float density = context.getResources().getDisplayMetrics().density;
        return (int) (dipValue * density + 0.5f);
    }
}
