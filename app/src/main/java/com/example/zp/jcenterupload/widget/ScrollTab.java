package com.example.zp.jcenterupload.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;

public class ScrollTab extends HorizontalScrollView {

    private LinearLayout tabContainer;
    private OnClickListenerTab mOnTabClickListener;
    private View mLastTabView;
    private OnTabSelectedListener mOnTabSelectedListener;
    private int INVALID_POSITION = -1;
    private static int DEFAULT_SELECT = 0;
    private int mPosition = DEFAULT_SELECT;

    public ScrollTab(Context context) {
        super(context);
        init(context);
    }

    public ScrollTab(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ScrollTab(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(21)
    public ScrollTab(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    private void init(Context context) {
        setHorizontalScrollBarEnabled(false);
        mOnTabClickListener = new OnClickListenerTab();
        tabContainer = new LinearLayout(context);
        tabContainer.setHorizontalGravity(LinearLayout.HORIZONTAL);
        tabContainer.setGravity(Gravity.CENTER_VERTICAL);
        addView(tabContainer, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }

    @Override
    public void addView(View child) {
        if (getChildCount() > 0) {
            tabContainer.addView(child);
            child.setOnClickListener(mOnTabClickListener);
        } else {
            super.addView(child);
        }
    }

    @Override
    public void addView(View child, int index) {
        if (getChildCount() > 0) {
            tabContainer.addView(child, index);
            child.setOnClickListener(mOnTabClickListener);
        } else {
            super.addView(child, index);
        }
    }

    @Override
    public void addView(View child, ViewGroup.LayoutParams params) {
        if (getChildCount() > 0) {
            tabContainer.addView(child, params);
            child.setOnClickListener(mOnTabClickListener);
        } else {
            super.addView(child, params);
        }
    }

    @Override
    public void addView(View child, int width, int height) {
        if (getChildCount() > 0) {
            tabContainer.addView(child, width, height);
            child.setOnClickListener(mOnTabClickListener);
        } else {
            super.addView(child, width, height);
        }
    }

    @Override
    public void addView(View child, int index, ViewGroup.LayoutParams params) {
        if (getChildCount() > 0) {
            tabContainer.addView(child, index, params);
            child.setOnClickListener(mOnTabClickListener);
        } else {
            super.addView(child, index, params);
        }
    }

    public int getPositionForView(View view) {
        View listItem = view;
        try {
            View v;
            while (!(v = (View) listItem.getParent()).equals(tabContainer)) {
                listItem = v;
            }
        } catch (ClassCastException e) {
            return INVALID_POSITION;
        }
        final int childCount = tabContainer.getChildCount();
        for (int i = 0; i < childCount; i++) {
            if (tabContainer.getChildAt(i).equals(listItem)) {
                return i;
            }
        }

        return INVALID_POSITION;
    }


    public void setTabSelect(int position) {
        if (mLastTabView != null) {
            mLastTabView.setSelected(false);
        }
        View v = null;
        try {
            v = tabContainer.getChildAt(position);
            if (v != null) {
                v.setSelected(true);
                onTabSelect(position, v);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            mLastTabView = v;
        }
    }

    private void onTabSelect(int position, View v) {
        if (mOnTabSelectedListener != null && mLastTabView != v) {
            mOnTabSelectedListener.onTabSelect(v, position);
        }
        int so = (int) ((getWidth() - v.getWidth()) / 2f);
        smoothScrollTo(v.getLeft() - so, 0);
    }

    class OnClickListenerTab implements OnClickListener {
        @Override
        public void onClick(View v) {
            mPosition = getPositionForView(v);
            setTabSelect(mPosition);
        }
    }


    /**
     *
     *
     */
    public interface OnTabSelectedListener {
        void onTabSelect(View v, int position);
    }

    public void setOnTabSelectedListener(OnTabSelectedListener listener) {
        this.mOnTabSelectedListener = listener;
    }

}
