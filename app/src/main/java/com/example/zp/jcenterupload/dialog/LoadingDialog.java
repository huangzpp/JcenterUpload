package com.example.zp.jcenterupload.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import com.example.zp.jcenterupload.R;
import com.wang.avi.AVLoadingIndicatorView;


public class LoadingDialog {
    private Dialog dialog;
    private final AVLoadingIndicatorView mAvi;

    public LoadingDialog(Context context) {
        dialog = new Dialog(context, R.style.app_dialog_theme_transparent);
        View contentview = LayoutInflater.from(context).inflate(R.layout.dialog_loading, null);
        mAvi = contentview.findViewById(R.id.avi);
        dialog.setContentView(contentview);
    }


    public void show() {
        mAvi.show();
        dialog.show();
    }

    public void dismiss() {
        mAvi.hide();
        dialog.dismiss();
    }

}
