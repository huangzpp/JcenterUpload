package com.example.zp.jcenterupload.sortrecyclerview;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

import com.example.zp.jcenterupload.R;
import com.example.zp.jcenterupload.base.ActionBarActivity;
import com.example.zp.jcenterupload.sortrecyclerview.adapter.UserAdapter;
import com.example.zp.jcenterupload.sortrecyclerview.bean.UserBean;
import com.pumpkin.navigation.ActionBarLayout;
import com.pumpkin.navigation.menu.TextMenu;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LinearRecyclerActivity extends ActionBarActivity{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_linear_recycler_view);
        RecyclerView recyclerView = findViewById(R.id.activity_linear_recycler_view);
        initRecyclerView(recyclerView);
    }

    private void initRecyclerView(RecyclerView recyclerView) {
        UserAdapter adapter = new UserAdapter(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        initExampleData(adapter,recyclerView);

    }
    private void initExampleData(final UserAdapter adapter,RecyclerView recyclerView){
        String[] args = new String[]{"你认真点好吗","不，可以吗","那你会死","死了又会怎么样","死了你就见不到我了","那我认真还不行吗","再长一点","再长一点点","还有吗","？？？","就先这样吧"};
        final List<UserBean> users = new ArrayList<>();
        for (int i = 0;i<args.length;i++){
            UserBean user = new UserBean();
            user.setIntro(args[i]);
            users.add(user);
        }
        adapter.setDatas(users);
        //为RecycleView绑定触摸事件
        ItemTouchHelper helper = new ItemTouchHelper(new ItemTouchHelper.Callback() {
            @Override
            public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                //首先回调的方法 返回int表示是否监听该方向
                int dragFlags = ItemTouchHelper.UP|ItemTouchHelper.DOWN;//拖拽
                int swipeFlags = ItemTouchHelper.LEFT|ItemTouchHelper.RIGHT;//侧滑删除
                return makeMovementFlags(dragFlags,swipeFlags);
            }

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                //滑动事件
                Collections.swap(users,viewHolder.getAdapterPosition(),target.getAdapterPosition());
                adapter.notifyItemMoved(viewHolder.getAdapterPosition(),target.getAdapterPosition());
                adapter.setDatas(users);
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                //侧滑事件
                users.remove(viewHolder.getAdapterPosition());
                adapter.notifyItemRemoved(viewHolder.getAdapterPosition());
            }


            @Override
            public boolean isLongPressDragEnabled() {
                //是否可拖拽
                return true;
            }
        });
        helper.attachToRecyclerView(recyclerView);
    }

    @Override
    protected void onCreateActionBar(com.pumpkin.navigation.ActionBar actionBar) {
        super.onCreateActionBar(actionBar);
        TextMenu marstMenu = new TextMenu(this);
        marstMenu.setText("RecyclerView");
        marstMenu.setTextColor(0xFFFFFFFF);
        marstMenu.setTextSize(18);
        marstMenu.setGravity(ActionBarLayout.Gravity.CENTER);
        marstMenu.setMarginLeft(10);
        actionBar.setCustomMenu(marstMenu);
    }

}
