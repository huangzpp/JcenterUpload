package com.example.zp.jcenterupload.http;

import android.text.TextUtils;

import com.alibaba.fastjson.JSON;
import com.leaf.u.basehttp.http.inter.ILeafRequest;
import com.leaf.u.basehttp.http.okimpl.LeafRequest;

import java.util.HashMap;
import java.util.Map;

import okhttp3.Request;

public class JsonDataRequest extends LeafRequest {


    private String path;
    private Map<String, Object> jsonMap = new HashMap<>();

    public JsonDataRequest putJsonData(String key, Object value) {
        jsonMap.put(key, value);
        return this;
    }

    @Override
    public JsonDataRequest post() {
        super.post();
        setContentType(LeafRequest.FORM + "; charset=utf-8");
        return this;
    }

    public JsonDataRequest path(String path) {
        this.path = path;
        return this;
    }

    @Override
    public LeafRequest url(String url) {
        return super.url(url);
    }

    @Override
    public String getUrl() {
        if (TextUtils.isEmpty(super.getUrl())) {
            return Constants.URL + path;
        }
        return super.getUrl();
    }

    @Override
    public Request build() {
        String json = JSON.toJSONString(jsonMap);
        put("data", json);
        return super.build();
    }
}
