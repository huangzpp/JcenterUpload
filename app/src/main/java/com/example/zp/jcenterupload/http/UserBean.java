package com.example.zp.jcenterupload.http;

public class UserBean {
    String name;
    String age;

    public UserBean() {
    }

    public UserBean(String name, String age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
}
