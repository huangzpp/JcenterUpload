package com.example.zp.jcenterupload.sortrecyclerview.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.zp.jcenterupload.R;
import com.example.zp.jcenterupload.sortrecyclerview.bean.UserBean;
import com.pumpkin.baseui.BaseRecycleAdapter;

public class UserAdapter extends BaseRecycleAdapter<UserBean, UserAdapter.ViewHolder> {
    public UserAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.item_recycler_user, null);
        ViewHolder holder = new ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.update(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView intro;
        private final ImageView icon;

        public ViewHolder(View itemView) {
            super(itemView);
            intro = itemView.findViewById(R.id.item_text_intro);
            icon = itemView.findViewById(R.id.item_img_icon);
        }

        public void update(int position) {
            UserBean userBean = get(position);
            intro.setText(userBean.getIntro());
        }
    }
}
