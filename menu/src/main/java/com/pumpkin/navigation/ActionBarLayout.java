package com.pumpkin.navigation;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zp on 2017/10/31.
 */

public class ActionBarLayout extends ViewGroup {
    public enum Gravity {
        LEFT,
        CENTER,
        RIGHT
    }

    private List<View> marstMenuList = new ArrayList<>();//左边menu列表
    private List<View> itemMenuList = new ArrayList<>();//右边menu列表
    private List<View> overFlowList = new ArrayList<>();
    private View customView;
    private int itemLimit = 2;
//    f70bb5913795ef53cf100cbb22fda1a6

    public ActionBarLayout(Context context) {
        super(context);
    }

    public ActionBarLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ActionBarLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(21)
    public ActionBarLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onLayout(boolean changed,
                            int l, int t, int r, int b) {
        int left = l;
        for (View menu : marstMenuList) {
            int measuredWidth = menu.getMeasuredWidth();
            LayoutParams layoutParams = (LayoutParams) menu.getLayoutParams();
            int marginLeft = layoutParams.leftMargin;
            int marginRight = layoutParams.rightMargin;
            int marginTop = layoutParams.topMargin;
            int marginBottom = layoutParams.bottomMargin;
            left += marginLeft;
            menu.layout(left, t + marginTop, left + measuredWidth, b - marginBottom);
            left += (measuredWidth + marginRight);
        }
        int right = r;
        for (int i = itemMenuList.size() - 1; i >= 0; i--) {
            View menu = itemMenuList.get(i);
            int measuredWidth = menu.getMeasuredWidth();
            LayoutParams layoutParams = (LayoutParams) menu.getLayoutParams();
            int marginLeft = layoutParams.leftMargin;
            int marginRight = layoutParams.rightMargin;
            int marginTop = layoutParams.topMargin;
            int marginBottom = layoutParams.bottomMargin;
            right -= marginRight;
            menu.layout(right - measuredWidth, t, right, b);
            right = right - measuredWidth - marginLeft;
        }
        if (customView != null) {
            LayoutParams layoutParams = (LayoutParams) customView.getLayoutParams();
            Gravity gravity = layoutParams.gravity;
            int marstMenuWidth = customView.getMeasuredWidth();
            int marstLeft = 0;
            int marstTop = t;
            int marstRight = 0;
            int marstBottom = b;
            switch (gravity) {
                case CENTER:
                    marstLeft = r / 2 - marstMenuWidth / 2;
                    if (marstLeft <= left + layoutParams.leftMargin) {
                        marstLeft = left + layoutParams.leftMargin;
                    }
                    marstRight = marstLeft + customView.getMeasuredWidth();
                    if (marstRight >= right - layoutParams.rightMargin) {
                        marstRight = right - layoutParams.rightMargin;
                    }
                    break;
                case LEFT:
                    marstLeft = left + layoutParams.leftMargin;
                    marstRight = marstLeft + customView.getMeasuredWidth();
                    if (marstRight >= right - layoutParams.rightMargin) {
                        marstRight = right - layoutParams.rightMargin;
                    }
                    break;
                case RIGHT:
                    marstRight = right - layoutParams.rightMargin;
                    marstLeft = marstRight - customView.getMeasuredWidth();
                    if (marstLeft <= left + layoutParams.leftMargin) {
                        marstLeft = left + layoutParams.leftMargin;
                    }
                    break;
            }
            customView.layout(marstLeft, marstTop, marstRight, marstBottom);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        int drawWidthSpacehalfLeft = width / 2;
        int drawWidthSpacehalfRight = width / 2;
//
//

        for (int i = 0; i < marstMenuList.size(); i++) {
            View view = marstMenuList.get(i);
            LayoutParams params = (LayoutParams) view.getLayoutParams();
            int childWidthMeasureSpec = getChildMeasureSpec(widthMeasureSpec, 0, ViewGroup.LayoutParams.WRAP_CONTENT);
            int childHeightMeasureSpec = getChildMeasureSpec(heightMeasureSpec, 0, ViewGroup.LayoutParams.WRAP_CONTENT);
            view.measure(childWidthMeasureSpec, childHeightMeasureSpec);
            drawWidthSpacehalfLeft -= (view.getMeasuredWidth() + params.leftMargin + params.rightMargin);
        }
        for (int i = 0; i < itemMenuList.size(); i++) {
            View view = itemMenuList.get(i);
            LayoutParams params = (LayoutParams) view.getLayoutParams();
            int childWidthMeasureSpec = getChildMeasureSpec(widthMeasureSpec, 0, ViewGroup.LayoutParams.WRAP_CONTENT);
            int childHeightMeasureSpec = getChildMeasureSpec(heightMeasureSpec, 0, ViewGroup.LayoutParams.WRAP_CONTENT);
            view.measure(childWidthMeasureSpec, childHeightMeasureSpec);
            drawWidthSpacehalfRight -= (view.getMeasuredWidth() + params.leftMargin + params.rightMargin);
        }
        if (customView != null) {
            customView.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
            int measuredWidth = customView.getMeasuredWidth();
            LayoutParams params = (LayoutParams) customView.getLayoutParams();
            Gravity gravity = params.gravity;
            switch (gravity) {
                case LEFT:
                case RIGHT:
                    if (measuredWidth > drawWidthSpacehalfLeft + drawWidthSpacehalfRight) {
                        measuredWidth = drawWidthSpacehalfLeft + drawWidthSpacehalfRight;
                    }
                    break;
                case CENTER:
                    int minhalfSpace = drawWidthSpacehalfLeft < drawWidthSpacehalfRight ? drawWidthSpacehalfLeft : drawWidthSpacehalfRight;
                    if (minhalfSpace * 2 < measuredWidth) {
                        measuredWidth = minhalfSpace * 2;
                    }
                    break;
            }
            int childWidthMeasureSpec = getChildMeasureSpec(widthMeasureSpec, 0, measuredWidth);
            int childHeightMeasureSpec = getChildMeasureSpec(heightMeasureSpec, 0, ViewGroup.LayoutParams.WRAP_CONTENT);
            customView.measure(childWidthMeasureSpec, childHeightMeasureSpec);
        }
        setMeasuredDimension(width, height);
    }

    @Override
    public void addView(View child) {
        super.addView(child);
    }

    @Override
    public void addView(View child, int index) {
        super.addView(child, index);
    }

    @Override
    public void addView(View child, int width, int height) {
        super.addView(child, width, height);
    }

    @Override
    public void addView(View child, ViewGroup.LayoutParams params) {
        super.addView(child, params);
    }

    @Override
    public void addView(View child, int index, ViewGroup.LayoutParams params) {
        super.addView(child, index, params);
    }

    @Override
    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attrs) {
        return super.generateLayoutParams(attrs);
    }

    @Override
    protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams p) {
        return super.generateLayoutParams(p);
    }

    @Override
    protected ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return super.generateDefaultLayoutParams();
    }

    public void setCustomMenu(View view, Gravity gravity) {
        LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params.setGravity(gravity);
        customView = view;
        addView(view, params);
    }

    public void addMenuItem(View view, Gravity gravity) {
        LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params.setGravity(gravity);
        addView(view, params);
        switch (gravity) {
            case CENTER:

                break;
            case LEFT:
                marstMenuList.add(view);
                break;
            case RIGHT:
                itemMenuList.add(view);
                break;
        }
    }

    public void setCustomMenu(View menu) {
        LayoutParams params = (LayoutParams) menu.getLayoutParams();
        if (params == null) {
            params = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }
        if (customView != null) {
            removeView(customView);
        }
        customView = menu;
        addView(menu, params);
    }

    public void addMenuItem(View menu) {
        LayoutParams params = (LayoutParams) menu.getLayoutParams();
        if (params == null) {
            params = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }
        Gravity gravity = params.gravity;
        addView(menu, params);
        switch (gravity) {
            case CENTER:
                setCustomMenu(menu);
                break;
            case LEFT:
                marstMenuList.add(menu);
                break;
            case RIGHT:
                itemMenuList.add(menu);
                break;
        }
    }

    public static class LayoutParams extends ViewGroup.MarginLayoutParams {

        private int type;
        private Gravity gravity = Gravity.RIGHT;
        private boolean isHide;


        public void setGravity(Gravity gravity) {
            this.gravity = gravity;
        }

        public LayoutParams(Context c, AttributeSet attrs) {
            super(c, attrs);
        }

        public LayoutParams(int width, int height) {
            super(width, height);
        }

        public LayoutParams(ViewGroup.LayoutParams source) {
            super(source);
        }
    }
}
