package com.pumpkin.navigation;

import android.support.annotation.ColorInt;

/**
 * Created by zp on 2017/11/2.
 */

public class ActionBar {
    private ActionBarLayout mActionbarView;

    public ActionBar(ActionBarLayout actionBarLayout) {
        this.mActionbarView = actionBarLayout;
    }

    public void setBackgroundColor(@ColorInt int color) {
        mActionbarView.setBackgroundColor(color);
    }

    public void addMenu(MenuItem item) {
        mActionbarView.addMenuItem(item.getView());
    }

    public void setCustomMenu(MenuItem menu) {
        mActionbarView.setCustomMenu(menu.getView());
    }

}
