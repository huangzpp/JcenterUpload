package com.pumpkin.navigation;

import android.content.Context;
import android.support.annotation.ColorInt;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by zp on 2017/11/2.
 */

public abstract class MenuItem {
    private LayoutInflater mInflater;
    private ActionBarLayout.LayoutParams layoutParams;
    private View mView;
    private final float density;

    public MenuItem(Context context) {
        layoutParams = new ActionBarLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mInflater = LayoutInflater.from(context);
        mView = onCreateMenu(mInflater);
        mView.setLayoutParams(layoutParams);
        density = context.getResources().getDisplayMetrics().density;
    }

    public int getMarginLeft() {
        return layoutParams.leftMargin;
    }

    public int getMarginRight() {
        return layoutParams.rightMargin;
    }

    public int getMarginTop() {
        return layoutParams.topMargin;
    }

    public int getMarginBottom() {
        return layoutParams.bottomMargin;
    }

    public int getPaddingLeft() {
        return mView.getPaddingLeft();
    }

    public int getPaddingRight() {
        return mView.getPaddingRight();
    }

    public int getPaddingTop() {
        return mView.getPaddingTop();
    }

    public int getPaddingBottom() {
        return mView.getPaddingBottom();
    }

    public void setPaddingLeft(int paddingLeftdp) {
        mView.setPadding(dp2pix(paddingLeftdp), 0, 0, 0);
    }

    public void setPaddingRight(int paddingRightdp) {
        mView.setPadding(0, 0, dp2pix(paddingRightdp), 0);
    }

    public void setPaddingTop(int paddingTopdp) {
        mView.setPadding(0, dp2pix(paddingTopdp), 0, 0);
    }

    public void setPaddingBottom(int paddingBottomdp) {
        mView.setPadding(0, 0, 0, dp2pix(paddingBottomdp));
    }

    public void setMarginLeft(int marginLeftdp) {
        layoutParams.leftMargin = dp2pix(marginLeftdp);
    }

    public void setMarginTop(int marginTopdp) {
        layoutParams.topMargin = dp2pix(marginTopdp);
    }

    public void setMarginRight(int marginRightdp) {
        layoutParams.rightMargin = dp2pix(marginRightdp);
    }

    public void setMarginBottom(int marginBottom) {
        layoutParams.bottomMargin = dp2pix(marginBottom);
    }

    protected int dp2pix(int dp) {
        return (int) (dp * density);
    }

    public void setBackgroundResource(int resId) {
        mView.setBackgroundResource(resId);
    }

    public void setBackgroundColor(@ColorInt int color) {
        mView.setBackgroundColor(color);
    }

    public void setPadding(int leftdp, int topdp, int rightdp, int bottomdp) {
        mView.setPadding(dp2pix(leftdp), dp2pix(topdp), dp2pix(rightdp), dp2pix(bottomdp));
    }

    public void setMagin(int leftdp, int topdp, int rightdp, int bottomdp) {
        layoutParams.setMargins(dp2pix(leftdp), dp2pix(topdp), dp2pix(rightdp), dp2pix(bottomdp));
    }

    protected abstract View onCreateMenu(LayoutInflater inflater);

    public void setGravity(ActionBarLayout.Gravity gravity) {
        layoutParams.setGravity(gravity);
    }

    public void setOnclickListener(View.OnClickListener l) {
        mView.setOnClickListener(l);
    }

    public void setOnLongClickListener(View.OnLongClickListener l) {
        mView.setOnLongClickListener(l);
    }

    public View getView() {
        return mView;
    }
}
