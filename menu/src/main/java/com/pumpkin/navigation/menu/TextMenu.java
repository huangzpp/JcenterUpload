package com.pumpkin.navigation.menu;

import android.content.Context;
import android.support.annotation.ColorInt;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.pumpkin.navigation.MenuItem;
import com.pumpkin.tools.R;

/**
 * Created by zp on 2017/11/2.
 */

public class TextMenu extends MenuItem {
    private TextView mTextView;
    private View mContentView;

    public TextMenu(Context context) {
        super(context);
    }

    @Override
    protected View onCreateMenu(LayoutInflater inflater) {
        mContentView = inflater.inflate(R.layout.item_menu_text, null);
        mTextView = (TextView) mContentView.findViewById(R.id.menu_text);
        return mContentView;
    }

    public void setTextSize(float size) {
        mTextView.setTextSize(size);
    }

    public void setTextColor(@ColorInt int color) {
        mTextView.setTextColor(color);
    }

    public void setText(CharSequence text) {
        mTextView.setText(text);
    }
}
