package com.pumpkin.navigation.menu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.pumpkin.navigation.MenuItem;
import com.pumpkin.tools.R;

/**
 * Created by zp on 2017/11/2.
 */

public class ImageMenu extends MenuItem {
    LinearLayout.LayoutParams params;
    private View mContextView;
    private ImageView mImage;

    public ImageMenu(Context context) {
        super(context);
    }

    @Override
    protected View onCreateMenu(LayoutInflater inflater) {
        mContextView = inflater.inflate(R.layout.item_menu_img, null);
        mImage = (ImageView) mContextView.findViewById(R.id.menu_img);
        params = (LinearLayout.LayoutParams) mImage.getLayoutParams();
        return mContextView;
    }

    public void setImageSize(int widthdp, int heightdp) {
        params.height = dp2pix(heightdp);
        params.width = dp2pix(widthdp);
    }

    public void setImageResource(int resId) {
        mImage.setImageResource(resId);
    }
}
